package nl.jsprengers.vendingmachine;

/**
 * Here we will add test scenarios for the so-called happy flows, that is the normal operation of the machine.
 * Think of all the different input and outputs:
 * - Insert one or more different coins
 * - Choose any of the three beverages
 * - Receive change (or not)
 */
public class HappyFlowApiTest {

}
