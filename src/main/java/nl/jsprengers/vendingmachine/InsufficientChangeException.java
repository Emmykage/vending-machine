package nl.jsprengers.vendingmachine;

public class InsufficientChangeException extends Exception {

    public InsufficientChangeException(String message) {
        super(message);
    }

}
