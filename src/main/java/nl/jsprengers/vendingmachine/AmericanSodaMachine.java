package nl.jsprengers.vendingmachine;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AmericanSodaMachine implements VendingMachine {

    private final Inventory<AmericanCoin> cashInventory = new Inventory<AmericanCoin>();
    private final Inventory<Item> itemInventory = new Inventory<Item>();
    private int totalSales;
    private Item currentItem;
    private int currentBalance;

    public AmericanSodaMachine() {
        initialize();
    }

    private void initialize() {
        //initialize machine with 5 coins of each denomination
        //and 5 cans of each Item
        for (AmericanCoin c : AmericanCoin.values()) {
            cashInventory.put(c, 5);
        }

        for (Item i : Item.values()) {
            itemInventory.put(i, 5);
        }

    }

    @Override
    public long selectItemAndGetPrice(Item item) throws SoldOutException {
        if (itemInventory.hasItem(item)) {
            currentItem = item;
            return currentItem.getPrice();
        }
        throw new SoldOutException("Sold Out, Please buy another item");
    }

    @Override
    public void insertCoin(AmericanCoin coin) {
        currentBalance = currentBalance + coin.getDenomination();
        cashInventory.add(coin);
    }

    @Override
    public Output collectItemAndChange() throws InsufficientChangeException, InsufficientAmountInserted {
        Item item = collectItem();
        totalSales = totalSales + currentItem.getPrice();

        List<AmericanCoin> change = collectChange();

        return new Output(item, change);
    }

    private Item collectItem() throws InsufficientChangeException,
            InsufficientAmountInserted {
        if (isFullPaid()) {
            if (hasSufficientChange()) {
                itemInventory.deduct(currentItem);
                return currentItem;
            }
            throw new InsufficientChangeException("Not Sufficient change in Inventory");

        }
        long remainingBalance = currentItem.getPrice() - currentBalance;
        throw new InsufficientAmountInserted("Price not full paid, remaining : ",
                remainingBalance);
    }

    private List<AmericanCoin> collectChange() throws InsufficientChangeException {
        long changeAmount = currentBalance - currentItem.getPrice();
        List<AmericanCoin> change = getChange(changeAmount);
        updateCashInventory(change);
        currentBalance = 0;
        currentItem = null;
        return change;
    }

    @Override
    public List<AmericanCoin> refund() throws InsufficientChangeException {
        List<AmericanCoin> refund = getChange(currentBalance);
        updateCashInventory(refund);
        currentBalance = 0;
        currentItem = null;
        return refund;
    }


    private boolean isFullPaid() {
        return currentBalance >= currentItem.getPrice();
    }


    private List<AmericanCoin> getChange(long amount) throws InsufficientChangeException {
        List<AmericanCoin> changes = Collections.emptyList();

        if (amount > 0) {
            changes = new ArrayList<AmericanCoin>();
            long balance = amount;
            while (balance > 0) {
                if (balance >= AmericanCoin.QUARTER_25.getDenomination()
                        && cashInventory.hasItem(AmericanCoin.QUARTER_25)) {
                    changes.add(AmericanCoin.QUARTER_25);
                    balance = balance - AmericanCoin.QUARTER_25.getDenomination();
                } else if (balance >= AmericanCoin.DIME_10.getDenomination()
                        && cashInventory.hasItem(AmericanCoin.DIME_10)) {
                    changes.add(AmericanCoin.DIME_10);
                    balance = balance - AmericanCoin.DIME_10.getDenomination();
                } else if (balance >= AmericanCoin.NICKLE_5.getDenomination()
                        && cashInventory.hasItem(AmericanCoin.NICKLE_5)) {
                    changes.add(AmericanCoin.NICKLE_5);
                    balance = balance - AmericanCoin.NICKLE_5.getDenomination();
                } else if (balance >= AmericanCoin.PENNY_1.getDenomination()
                        && cashInventory.hasItem(AmericanCoin.PENNY_1)) {
                    changes.add(AmericanCoin.PENNY_1);
                    balance = balance - AmericanCoin.PENNY_1.getDenomination();
                } else {
                    throw new InsufficientChangeException("NotSufficientChange, Please try another product ");
                }
            }
        }

        return changes;
    }

    public void reset() {
        cashInventory.clear();
        itemInventory.clear();
        totalSales = 0;
        currentItem = null;
        currentBalance = 0;
    }

    private boolean hasSufficientChange() {
        return hasSufficientChangeForAmount(currentBalance - currentItem.getPrice());
    }

    private boolean hasSufficientChangeForAmount(long amount) {
        boolean hasChange = true;
        try {
            getChange(amount);
        } catch (InsufficientChangeException nsce) {
            return hasChange = false;
        }
        return hasChange;
    }

    private void updateCashInventory(List<AmericanCoin> change) {
        for (AmericanCoin c : change) {
            cashInventory.deduct(c);
        }
    }

    public long getTotalSales() {
        return totalSales;
    }

}
